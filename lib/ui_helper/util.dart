import 'package:flutter/material.dart';

TextStyle MyRiderTextStyle() {
  return TextStyle(fontSize: 13, color: Colors.black45);
}

TextStyle RideHeaderTextStyle() {
  return TextStyle(
      fontSize: 14, color: Colors.black, fontWeight: FontWeight.w500);
}

TextStyle HeadTextStyle() {
  return TextStyle(
      fontSize: 20, color: Color(0xff202244), fontWeight: FontWeight.bold);
}

TextStyle TripsTextStyle() {
  return TextStyle(fontWeight: FontWeight.bold, color: Color(0xffEDD500));
}

TextStyle NavTextStyle() {
  return TextStyle(fontWeight: FontWeight.bold, fontSize: 60);
}

TextStyle CardTextStyle() {
  return TextStyle(fontWeight: FontWeight.bold, fontSize: 15);
}

TextStyle TimeTextStyle() {
  return TextStyle(fontWeight: FontWeight.bold, fontSize: 12);
}

TextStyle ButtonTextStyle() {
  return TextStyle(color: Colors.white, fontSize: 20);
}
