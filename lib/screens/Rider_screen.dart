import "package:dotted_line/dotted_line.dart";
import "package:flutter/material.dart";
import "package:go_app/ui_helper/util.dart";
import "package:go_app/widgets/CustomWidget.dart";
import "package:go_app/widgets/button.dart";
import "package:go_app/widgets/containercard.dart";

class RiderScreen extends StatelessWidget {
  const RiderScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios),
        title: const Text("My Ride"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0, top: 5),
              child: Text(
                "You can now track your ride",
                style: MyRiderTextStyle(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 10.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      circleWithImage(
                        image: "location.png",
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        "Destination Details",
                        style: RideHeaderTextStyle(),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      DividerLine(),
                      Container1Card(),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 215, left: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      circleWithImage(
                        image: "8.png",
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        "Ride Information",
                        style: RideHeaderTextStyle(),
                      )
                    ],
                  ),
                  Row(
                    children: [DividerLine(), Container2Card()],
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 399, left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      circleWithImage(
                        image: "l3.png",
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        "Driver Information",
                        style: RideHeaderTextStyle(),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Container3Card(),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 550),
              child: Button(
                name: "View in Map",
              ),
            )
          ],
        ),
      ),
    );
  }
}
