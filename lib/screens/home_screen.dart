import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:go_app/ui_helper/util.dart';
import 'package:go_app/widgets/CustomWidget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

final List<String> imagePaths = [
  "assets/images/c1.jpg",
  "assets/images/c2.jpg",
  "assets/images/c3.jpg",
];
//for image to show
late List<Widget> _pages;
//for active
int _activePage = 0;
//for page controller
final PageController _pageController = new PageController(initialPage: 0);
//for actomatic timer
Timer? _timer;

class _HomeScreenState extends State<HomeScreen> {
  void startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 3), (timer) {
      if (_pageController.page == imagePaths.length - 1) {
        //check if its on the last image
        _pageController.animateToPage(0,
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeInOut);
      } else {
        _pageController.nextPage(
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeInOut);
      }
    });
  }

  void initState() {
    super.initState();
    _pages = List.generate(
        imagePaths.length,
        (index) => ImagePlaceHolder(
              imagePath: imagePaths[index],
            ));
    startTimer();
  }

  void dispose() {
    super.dispose();
    _timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: GestureDetector(
            //going back to home screen
            onTap: () {},
            child: Image.asset(
              "assets/images/icon.png",
            ),
          ),
          actions: [
            Container(
              // color: Colors.black,
              margin: EdgeInsets.only(right: 15),
              width: 32, // Increase the width to 64 (or any desired size)
              height: 32, // Increase the height to 64 (or any desired size)
              child: Image.asset(
                "assets/images/driver.png",
                fit: BoxFit.contain, // Adjust the fit as needed
              ),
            ),
          ],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              height: MediaQuery.of(context).size.height / 1.12,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Welcome,Prem!",
                    style: HeadTextStyle(),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'What service would you like to seek from ',
                        style: MyRiderTextStyle(),
                      ),
                      Row(
                        children: [
                          Text('365trips', style: TripsTextStyle()),
                          Text(' Today? ', style: MyRiderTextStyle()),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Column(
                    children: [
                      Stack(
                        children: [
                          SizedBox(
                            width: double.infinity,
                            height: MediaQuery.of(context).size.height / 3,
                            child: PageView.builder(
                                controller: _pageController,
                                itemCount: imagePaths.length,
                                onPageChanged: (value) {
                                  setState(() {
                                    _activePage = value;
                                  });
                                },
                                itemBuilder: (context, index) {
                                  //return image
                                  return _pages[index];
                                }),
                          ),
                          //code for page indicator
                          Positioned(
                            bottom: 10,
                            left: 0,
                            right: 0,
                            child: Container(
                              color: Colors.transparent,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: List.generate(
                                      _pages.length,
                                      (index) => Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5),
                                            child: InkWell(
                                              onTap: () {
                                                _pageController.animateToPage(
                                                    index,
                                                    duration: Duration(
                                                        milliseconds: 300),
                                                    curve: Curves.easeInOut);
                                              },
                                              child: CircleAvatar(
                                                radius: 4,
                                                backgroundColor:
                                                    _activePage == index
                                                        ? Color(0xffFF9900)
                                                        : Colors.white,
                                              ),
                                            ),
                                          ))),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Catagories", style: HeadTextStyle()),
                  SizedBox(
                    height: 15,
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: GridView(
                        // physics: NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10),
                        children: [
                          CardHome(image: "46.png", title: "RESERVATION"),
                          CardHome(image: "g1.png", title: "AIRPORT SERVICES"),
                          CardHome(image: "47.png", title: "TOUR PACKAGE"),
                          CardHome(image: "icon.png", title: "COMING SOON"),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

class ImagePlaceHolder extends StatelessWidget {
  final String? imagePath;
  const ImagePlaceHolder({super.key, this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      imagePath!,
      fit: BoxFit.cover,
    );
  }
}
