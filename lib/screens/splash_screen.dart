import 'dart:async';
import 'package:flutter/material.dart';
import 'package:go_app/componets/navigation_menu.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  late Animation colorAnimation;
  late Animation colortextAnimation;
  late AnimationController animationController;
  bool isFirst = true;

  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 3));
    colorAnimation =
        ColorTween(begin: const Color(0xffFFE6E6), end: const Color(0xffEFBC9B))
            .animate(animationController);
    colortextAnimation = ColorTween(begin: Colors.black, end: Colors.white)
        .animate(animationController);
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    Timer(Duration(seconds: 2), () {
      reload();
    });
    whereToGo();
  }

  void reload() {
    setState(() {
      isFirst = false;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: colorAnimation.value,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/images/icon.png"),
                SizedBox(
                  height: 50,
                ),
                Text(
                  "GO APP",
                  style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: colortextAnimation.value),
                ),
              ],
            ))));
  }

  void whereToGo() async {
    Timer(Duration(seconds: 4), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => NavigationMenu()));
    });
  }
}
