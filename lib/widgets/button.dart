import "package:flutter/material.dart";
import "package:go_app/ui_helper/util.dart";

class Button extends StatelessWidget {
  final String name;
  const Button({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          width: 332,
          height: 50,
          child: ElevatedButton(
            onPressed: () {},
            child: Center(
              child: Text(name, style: ButtonTextStyle()),
            ),
            style: ElevatedButton.styleFrom(
                backgroundColor: Color(0xffFF9900),
                shadowColor: Color(0xffFF9900),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(11)))),
          )),
    );
  }
}
