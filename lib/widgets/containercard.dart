import "package:dotted_line/dotted_line.dart";
import "package:flutter/material.dart";
import "package:go_app/ui_helper/util.dart";

class Container1Card extends StatelessWidget {
  const Container1Card({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 150,
        width: MediaQuery.of(context).size.width / 1.1,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, left: 10),
              child: Text(
                "25th March 2024,10:00am",
                style: MyRiderTextStyle(),
              ),
            ),
            const Divider(
              thickness: 1,
              color: Colors.grey,
              indent: 10,
              endIndent: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, left: 8),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset("assets/images/26.png"),
                  const SizedBox(
                    width: 36,
                  ),
                  Text("Airport,Biratnagar,Morang,Nepal",
                      overflow: TextOverflow.ellipsis)
                ],
              ),
            ),
            Container(
              height: 46,
              padding: EdgeInsets.only(left: 12.5),
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(),
              child: const DottedLine(
                direction: Axis.vertical,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, left: 10),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset("assets/images/261.png"),
                  const SizedBox(
                    width: 35,
                  ),
                  Text("Damak-5,Yalambar Chowk Jhapa,Nepal")
                ],
              ),
            ),
          ],
        ));
  }
}

class Container2Card extends StatelessWidget {
  const Container2Card({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: MediaQuery.of(context).size.width / 1.1,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 1.0, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset("assets/images/10.png"),
                Column(
                  children: [
                    Text(
                      "BA 2 PA 007",
                      style: TimeTextStyle(),
                    ),
                    const Text(
                      "Rs.200",
                      style: TextStyle(color: Color(0xffFF9900), fontSize: 20),
                    ),
                    Text(
                      "BA 2 PA 007",
                      style: TimeTextStyle(),
                    )
                  ],
                )
              ],
            ),
          ),
          const Divider(
            thickness: 1,
            color: Colors.grey,
            indent: 10,
            endIndent: 20,
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  children: [
                    Image.asset("assets/images/Vector.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Text(
                          "4",
                          style: TimeTextStyle(),
                        ),
                        Text(
                          "wheeler",
                          style: TimeTextStyle(),
                        )
                      ],
                    )
                  ],
                ),
              ),
              const SizedBox(
                width: 100,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  children: [
                    Image.asset("assets/images/l3.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Text(
                          "4",
                          style: TimeTextStyle(),
                        ),
                        Text(
                          "wheeler",
                          style: TimeTextStyle(),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class Container3Card extends StatelessWidget {
  const Container3Card({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 84,
      width: MediaQuery.of(context).size.width / 1.15,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset("assets/images/user.png"),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "John Doe",
                style: TimeTextStyle(),
              ),
              Text(
                "Biratnagar",
                style: MyRiderTextStyle(),
              )
            ],
          ),
          const SizedBox(
            width: 100,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Image.asset("assets/images/btn_call.png"), Text("Call")],
          )
        ],
      ),
    );
  }
}

class DividerLine extends StatelessWidget {
  const DividerLine({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 10, left: 5),
        height: 150,
        child: VerticalDivider(
          color: Colors.grey,
        ));
  }
}
