import "package:flutter/material.dart";
import "package:go_app/ui_helper/util.dart";

class circleWithImage extends StatelessWidget {
  final String image;
  circleWithImage({required this.image});
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      width: 25,
      height: 25,
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 5,
                spreadRadius: 2,
                offset: const Offset(0, 3))
          ]),
      child: ClipOval(
        child: Center(
          // Center the icon
          child: Image.asset(
            "assets/images/" + image,
            width: 15, // Increased width for a bigger icon
            height: 20, // Increased height for a bigger icon
          ),
        ),
      ),
    );
  }
}

// class CardHome extends StatelessWidget {
//   final String image;
//   final String title;
//   CardHome({
//     required this.image,
//     required this.title,
//   });
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//           width: double.infinity,
//           height: double.infinity,
//           decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(20.0),
//               boxShadow: [
//                 BoxShadow(
//                     color: Color.fromARGB(245, 238, 233, 233).withOpacity(0.5),
//                     blurRadius: 5,
//                     spreadRadius: 2,
//                     offset: const Offset(0, 3))
//               ]),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Image.asset(
//                 "assets/images/" + image,
//                 height: 80,
//                 width: 100.01,
//               ),
//               SizedBox(height: 20),
//               Text(
//                 title,
//                 style: CardTextStyle(),
//               )
//             ],
//           )),
//     );
//   }
// }
class CardHome extends StatelessWidget {
  final String image;
  final String title;
  CardHome({
    required this.image,
    required this.title,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Card(
          elevation: 1,
          // shape: ShapeBorder.lerp(Border.symmetric(horizontal: 2), 3.0, t),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/" + image,
                  height: 80,
                  width: 100.01,
                ),
                SizedBox(height: 20),
                Text(
                  title,
                  style: CardTextStyle(),
                )
              ],
            ),
          )),
    );
  }
}
