import "package:flutter/material.dart";
import "package:go_app/screens/Rider_screen.dart";
import "package:go_app/screens/home_screen.dart";
import "package:go_app/ui_helper/util.dart";

class NavigationMenu extends StatefulWidget {
  const NavigationMenu({super.key});

  @override
  State<NavigationMenu> createState() => _NavigationMenuState();
}

class _NavigationMenuState extends State<NavigationMenu> {
  int index = 0;
  final screens = [
    HomeScreen(),
    Center(
      child: Text(
        "HISTORY",
        style: NavTextStyle(),
      ),
    ),
    // MyRidesScreen(),
    RiderScreen(),
    Center(
      child: Text(
        "SETTINGS",
        style: NavTextStyle(),
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[index],
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(indicatorColor: Color(0xffFF9900)),
        child: NavigationBar(
          height: 60,
          animationDuration: Duration(seconds: 2),
          selectedIndex: index,
          onDestinationSelected: (index) => setState(() => this.index = index),
          destinations: [
            NavigationDestination(
              icon: Icon(Icons.home_outlined),
              label: 'HOME',
              selectedIcon: Icon(Icons.home),
            ),
            NavigationDestination(
              icon: Icon(Icons.book_outlined),
              label: 'HISTORY',
              selectedIcon: Icon(Icons.book),
            ),
            NavigationDestination(
              icon: Icon(Icons.web_asset_outlined),
              label: 'MY RIDES',
              selectedIcon: Icon(Icons.web_asset),
            ),
            NavigationDestination(
              icon: Icon(Icons.settings_outlined),
              label: 'SETTINGS',
              selectedIcon: Icon(Icons.settings),
            ),
          ],
        ),
      ),
    );
  }
}
