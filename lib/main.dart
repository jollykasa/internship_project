import 'package:flutter/material.dart';
import 'package:go_app/componets/navigation_menu.dart';
import 'package:go_app/screens/home_screen.dart';

import 'package:go_app/screens/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          //app bar
          appBarTheme: const AppBarTheme(
        centerTitle: true,
        elevation: 1.0,
        iconTheme: IconThemeData(color: Colors.black),
        titleTextStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24),
        // backgroundColor: Colors.grey,
      )),
      home: SplashScreen(),
    );
  }
}
